Example Electrum files for the paper (under submission) "A Bounded Domain Property for a Pragmatic Fragment of First-Order Linear Temporal Logic" by Quentin Peyras, Julien Brunel and David Chemouil (ONERA DTIS and Université fédérale de Toulouse).

The Electrum Analyzer can be downloaded from <http://haslab.github.io/Electrum/>.

(C) ONERA DTIS 2019.